"""Application exporter"""

import os
import time
from prometheus_client import start_http_server, Gauge, Enum
import requests
import http.client
import json

class AuthMetrics:
    """
    Representation of Prometheus metrics and loop to fetch and transform
    application metrics into Prometheus metrics.
    """

    def __init__(self, polling_interval_seconds=5, client_id=None, client_secret=None, auth_domain=None, api_path=None):
        # self.app_port = app_port
        self.polling_interval_seconds = polling_interval_seconds
        self.client_id = client_id
        self.client_secret = client_secret
        self.auth_domain = auth_domain
        self.api_path = api_path
        # Prometheus metrics to collect
        # self.current_requests = Gauge("app_requests_current", "Current requests")
        # self.pending_requests = Gauge("app_requests_pending", "Pending requests")
        # self.total_uptime = Gauge("app_uptime", "Uptime")
        # self.health = Enum("app_health", "Health", states=["healthy", "unhealthy"])

        # OIDC Metrics
        self.token_health = Enum("oidc_access_health", "Health", states=["healthy", "unhealthy"])
        self.auth_health = Enum("oidc_auth_health", "Health", states=["healthy", "unhealthy"])
        self.access_response_time = Gauge("oidc_access_response_time", "Access token response time")
        self.auth_response_time = Gauge("oidc_auth_response_time", "Auth token response time")
        
    def run_metrics_loop(self):
        """Metrics fetching loop"""

        while True:
            self.oidc_access_token(client_id=self.client_id, client_secret=self.client_secret, auth_domain=self.auth_domain)
            if self.token_health_state == "healthy":
                self.oidc_auth_token(self.auth_domain)
            else:
                self.auth_health.state("unhealthy")
                print(time.asctime(time.localtime(time.time())) + " - access token request failed, thus skipping auth token")
            time.sleep(self.polling_interval_seconds)

    def oidc_access_token(self, client_id, client_secret, auth_domain):
        """
        Check if OIDC tenant responds with json data
        """


        conn = http.client.HTTPSConnection(auth_domain)

        payload = "{\"client_id\":\"" + client_id + "\",\"client_secret\":\"" + client_secret + "\",\"audience\":\"" + "https://" + auth_domain + self.api_path + "\",\"grant_type\":\"client_credentials\"}"

        headers = { 'content-type': "application/json" }

        conn.request("POST", "/oauth/token", payload, headers)

        rta = time.time()
        res = conn.getresponse()
        rtb = time.time()
        self.access_response_time.set(round(rtb - rta, 3))
        data = res.read()
        print(time.asctime(time.localtime(time.time())) + " - Access token request status received: " + str(res.status))
        if res.status == 200:
            self.token_health_state = "healthy"
            self.access_token = json.loads(data)['access_token']
        else:
            self.token_health_state = "unhealthy"
        print(time.asctime(time.localtime(time.time())) + " - access token state is " + self.token_health_state.upper() + " (after " + str(round(rtb - rta, 3)) + " seconds)")
        self.token_health.state(self.token_health_state)

    def oidc_auth_token(self, auth_domain):

        conn = http.client.HTTPSConnection(auth_domain)
        headers = { 'authorization': "Bearer " + self.access_token }
        conn.request("GET", self.api_path, headers=headers)

        rta = time.time()
        res = conn.getresponse()
        rtb = time.time()
        self.auth_response_time.set(round(rtb - rta, 3))
        # data = res.read()

        if res.status == 404:
            self.auth_health_state = "healthy"
        else:
            self.auth_health_state = "unhealthy"
        
        print(time.asctime(time.localtime(time.time())) + " - auth token state is " + self.auth_health_state.upper() + " (after " + str(round(rtb - rta, 3)) + " seconds)")
        self.auth_health.state(self.auth_health_state)

def main():
    """Main entry point"""

    polling_interval_seconds = int(os.getenv("POLLING_INTERVAL_SECONDS", "60"))
    # app_port = int(os.getenv("APP_PORT", "80"))
    exporter_port = int(os.getenv("EXPORTER_PORT", "9877"))
    client_id = os.getenv("CLIENT_ID")
    client_secret = os.getenv("CLIENT_SECRET")
    auth_domain = os.getenv("AUTH_DOMAIN")
    api_path = os.getenv("API_PATH", "/api/v2/")

    app_metrics = AuthMetrics(
        # app_port=app_port,
        polling_interval_seconds=polling_interval_seconds,
        client_id=client_id,
        client_secret=client_secret,
        auth_domain=auth_domain,
        api_path=api_path
    )
    start_http_server(exporter_port)
    app_metrics.run_metrics_loop()

if __name__ == "__main__":
    main()