FROM python:3.6.4-alpine3.6

COPY ./Python/oidc-exporter.py /etc/code/

# ARG client_id
# ENV CLIENT_ID=${client_id}

# ARG client_secret
# ENV CLIENT_SECRET=${client_secret}

# ARG auth_domain
# ENV AUTH_DOMAIN=${auth_domain}

# ARG exporter_port=9877
# ENV EXPORTER_PORT=${exporter_port}

# ARG polling_interval_seconds=60
# ENV POLLING_INTERVAL_SECONDS=${polling_interval_seconds}

# ARG api_path="/api/v2/"
# ENV API_PATH=${api_path}

RUN pip install prometheus_client
RUN pip install requests

CMD ["python", "/etc/code/oidc-exporter.py"]